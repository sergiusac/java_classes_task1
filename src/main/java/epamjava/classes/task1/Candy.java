package epamjava.classes.task1;

public class Candy implements Comparable<Candy> {
    private String name;
    private double weight;
    private double sugarConcentration;
    private double price;

    public Candy() {
    }

    public Candy(String name, double weight, double sugarConcentration, double price) {
        this.name = name;
        this.weight = weight;
        this.sugarConcentration = sugarConcentration;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getSugarConcentration() {
        return sugarConcentration;
    }

    public void setSugarConcentration(double sugarConcentration) {
        this.sugarConcentration = sugarConcentration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int compareTo(Candy other) {
        return Double.compare(price, other.price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Candy candy = (Candy) o;

        if (Double.compare(candy.weight, weight) != 0) return false;
        if (Double.compare(candy.sugarConcentration, sugarConcentration) != 0) return false;
        if (Double.compare(candy.price, price) != 0) return false;
        return name != null ? name.equals(candy.name) : candy.name == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sugarConcentration);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.format(
                "[Name %s; Weight %f; Sugar %f; Price %f]",
                name, weight, sugarConcentration, price
        );
    }
}
