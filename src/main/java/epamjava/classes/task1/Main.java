package epamjava.classes.task1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<Candy> candies = new TreeSet<>();
        candies.add(new Candy("Mars", 0.5, 0.25, 500));
        candies.add(new Candy("Mars", 0.5, 0.25, 500));
        candies.add(new Candy("Albeni", 1.5, 0.27, 700));
        candies.add(new Candy("Twix", 0.4, 0.5, 450));
        candies.add(new Candy("Twix", 0.4, 0.5, 450));
        candies.add(new Candy("Twix", 0.4, 0.5, 450));
        candies.add(new Candy("Snickers", 0.3, 0.55, 470));
        candies.add(new Candy("MM", 1.0, 0.3, 300));

        Gift gift = new Gift(candies);

        System.out.println(String.format("Weight of the gift - %.2f", gift.calculateWeight()));
        System.out.println(String.format("Price of the gift - %.2f", gift.calculatePrice()));

        List<Candy> sortedCandies = gift.sortCandiesByPrice(false);

        System.out.println("Candies in this gift");
        for (Candy candy : gift.getCandies()) {
            System.out.println(candy);
        }

        System.out.println("Candies sorted by price");
        for (Candy candy : sortedCandies) {
            System.out.println(candy);
        }

        double from = 0.26;
        double to = 0.29;
        Optional<Candy> foundCandy = gift.findCandyWithSugarConcentration(from, to);
        if (foundCandy.isPresent()) {
            System.out.println(String.format(
                    "Candy with sugar concentration from %.2f to %.2f - %s",
                    from, to, foundCandy.get()));
        } else {
            System.out.println("Could not find the appropriate candy");
        }
    }
}
